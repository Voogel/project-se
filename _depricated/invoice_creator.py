from fpdf import FPDF
import os
from random import randint
from dangerous_corpus import dangerous_corpus
import pandas as pd

os.chdir(r'D:\School\ProjectSE\Python\model\invoices_veel')

normal_words = [
    "chair", "couch", "water", "tree", "lotion", "fire",
    "pony", "computer", "mouse", "butter", "green", "red", 
    "blue", "purple", "light", "pills", "phone", "cups",
    "typewriter", "file", "horse", "glass", "plant", "poster", 
    "supplements", "charger", "cord", "lightbulb", "sage", "basil",
    "bicycle", "nintendo", "raining", "invoice", "payment", "euros",
    "dollars", "yen", "funtime", "hat", "motorbike", "sight",
    "puzzle", "bush", "weight", "straps", "barbell", "dumbell",
    "elastic", "plaster", "wall", "clerk", "pants", "shirt",
    "pig", "plants", "tube", "leaf", "heel", "keyboard",
    "spinach", "apple", "leeks", "hyena", "hay", "wheat"
]
dangerous_words = [
    "gun",
    "knife",
    "weapon",
    "missile",
    "ammunition",
    "bullet",
    "sword",
    "rifle",
    "pack"
    "bomb"]

def create_dangerous_sentence():
    sentence = ""
    i = 0
    dangerous_word_pos = randint(0, 4)
    while i < 5:
        if i == dangerous_word_pos:
            sentence += dangerous_corpus[randint(0, (len(dangerous_corpus)-1))] + " "
        else:
            sentence += normal_words[randint(0, (len(normal_words)-1))] + " "
        i+=1
    return sentence

def create_normal_sentence():
    sentence = ""
    i = 0
    while i < 5:
        sentence += normal_words[randint(0, (len(normal_words)-1))] + " "
        i+=1
    return sentence


# i = 0
# while i < 10000:
#     dangerous_doc = True if i % 10 == 0 else False
#     pdf = FPDF()
#     pdf.add_page()
#     pdf.set_font('Arial', 'B', 11)
    
#     sentence = 0
#     max_sentence = 20
#     dangerous_sentence_pos = [randint(0, 19), randint(0, 19), randint(0, 19), randint(0, 19)]

#     while sentence < max_sentence:
#         if dangerous_doc is True:
#             if sentence in dangerous_sentence_pos:
#                 pdf.cell(w= 40, h = 10, txt = create_dangerous_sentence(), ln = 1)
#             else:
#                 pdf.cell(w= 40, h = 10, txt = create_normal_sentence(), ln = 1)
#         else:
#             pdf.cell(w= 40, h = 10, txt = create_normal_sentence(), ln = 1)

#         sentence+=1
    
#     pdf_name = 'doc_' + str(randint(0, 2147483640))+ '.pdf'
#     pdf.output(pdf_name, 'F')
#     if i % 1000 == 0:
#         print(i)
#     i+=1




os.chdir(r'D:\School\ProjectSE\Python\model\dataframe')
df = pd.read_csv('labelled_invoices.csv')
df.drop(df.columns[[0,2]], axis = 1, inplace = True) 

print(df)

df.to_csv('UNlabelled_invoices.csv')
