import sys
import math
import os
import re
import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
import cv2
import socket
import pickle
import joblib
import pandas as pd
import matplotlib.pyplot as plt
import nltk as nltk
import time
# nltk.download('punkt')
# nltk.download('stopwords')
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.layout import LAParams, LTTextBox, LTTextLine
from pdfminer.converter import PDFPageAggregator
from PyPDF2 import PdfFileReader
from unipath import Path
from dangerous_corpus import dangerous_corpus
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB
from pathlib import Path

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Extract text from all .jpg, .png, and .pdf files in directory
# RETURNS: list
def extract_text_from_invoices(invoice_directory):
    invoices = []
    for invoice in os.listdir(invoice_directory):
        invoice_filepath = os.path.join(invoice_directory, invoice)
        invoice_abs_filepath = os.path.abspath(invoice_filepath)

        if invoice_abs_filepath.endswith(".jpg") or invoice_abs_filepath.endswith(".png"):
            invoice_img = cv2.imread(invoice_abs_filepath)
            invoice_text = pytesseract.image_to_string(invoice_img)
            invoices.append(invoice_text)
        
        elif invoice_abs_filepath.endswith(".pdf"):
            with open(invoice_abs_filepath, 'rb') as f:
                pdf = PdfFileReader(f)
                amount_of_pages = pdf.getNumPages()
                i = 0
                invoice_text = ""
                while i < amount_of_pages:
                    page = pdf.getPage(i)
                    invoice_text += page.extractText()
                    i+=1
                invoices.append(invoice_text)

    return invoices

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Tokenize list of strings
# RETURNS: list of lists
def tokenize_list(list):
    tokenized_list = []
    for item in list:
        tokenized_list.append(tokenize_string(item))
    return tokenized_list

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Tokenize single string
# RETURNS: list of strings
def tokenize_string(string):
    string = string.lower()
    words = word_tokenize(string)
    words = [w for w in words if len(w) > 2]
    sw = stopwords.words('english')
    words = [word for word in words if word not in sw]
    stemmer = PorterStemmer()
    words = [stemmer.stem(word) for word in words]   
    return words

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Label invoices in df as dangerous, based on words in dangerous_words.py
# RETURNS: void 
def create_labelled_invoices_dataframe(invoices, save):
    df = pd.DataFrame(invoices)
    df.rename(columns = {0: 'invoice'}, inplace = True)
    df['dangerous'] = 0

    for i, row in df.iterrows():
        if any(x in row['invoice'] for x in dangerous_corpus):
            df.at[i, 'dangerous'] = 1

    df.replace(to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"], value=["",""], regex=True, inplace=True)
    
    if save is True:
        df.to_csv(r'D:\School\ProjectSE\Python\model\dataframe\labelled_invoices.csv')
    
    return df


# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

start_time = time.time()


# Accept folder parameter
folder = Path(sys.argv[1])

# Extract text from invoices
invoices = extract_text_from_invoices(folder)

# create df
df = create_labelled_invoices_dataframe(invoices, save = False)
tokenized_invoices_column = tokenize_list(df['invoice'])
labels_column = df['dangerous']

# Create TF-IDF specifically for tokenized lists
def identity_tokenizer(text):
    return text
tfidf = TfidfVectorizer(
    analyzer = 'word',
    ngram_range = (1,2),
    tokenizer = identity_tokenizer,
    preprocessor = identity_tokenizer,
    token_pattern = None
)
tfidf.fit(tokenized_invoices_column)

# Split into train and test sets
vectorized_invoices = tfidf.transform(tokenized_invoices_column)
reviews_train, reviews_test, target_train, target_test = train_test_split(
    vectorized_invoices, 
    labels_column, 
    train_size = 0.75
)

# Train Naive Bayes model
model = MultinomialNB()
model.fit(reviews_train, target_train)

# Print model statistics for later use
elapsed_time = time.time() - start_time
print("Elapsed time:", elapsed_time, "seconds.")
print("Trained model on", math.floor(len(invoices)*0.75), "invoices.")
print("Tested model on", math.floor(len(invoices)*0.25), "invoices.")
print("Model accuracy:", model.score(reviews_test, target_test) * 100, "%")

# Save model & vectorizer using pickle:
os.chdir(r'D:\School\ProjectSE\Python\model')
with open('invoice_classifier.pickle', 'wb') as model_file:
    pickle.dump(model, model_file)
with open('tfidf.pickle', 'wb') as tfidf_file:
    pickle.dump(tfidf, tfidf_file)