import sys
import math
import os
import re
import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
import cv2
import socket
import pickle
import joblib
import pandas as pd
import matplotlib.pyplot as plt
import nltk as nltk
import time
import multiprocessing as mp
from nltk.tokenize import word_tokenize
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.layout import LAParams, LTTextBox, LTTextLine
from pdfminer.converter import PDFPageAggregator
from PyPDF2 import PdfFileReader
from unipath import Path
from dangerous_corpus import dangerous_corpus
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB
from shutil import copy2

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Tokenize list of strings
# RETURNS: listof lists
def tokenize_list(list):
    tokenized_list = []
    for item in list:
        tokenized_list.append(tokenize_string(item))
    return tokenized_list

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Tokenize single string
# RETURNS: list of strings
def tokenize_string(string):
    string = string.lower()
    words = word_tokenize(string)
    words = [w for w in words if len(w) > 2]
    sw = stopwords.words('english')
    words = [word for word in words if word not in sw]
    stemmer = PorterStemmer()
    words = [stemmer.stem(word) for word in words]   
    return words

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Added faux method for unpickling TFIDF vectorizer
def identity_tokenizer(text):
    return text

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Extract text from single invoice
# RETURNS: list [list of strings, list of strings]
def invoice_processor(invoice, invoice_directory):
        invoice_filepath = os.path.join(invoice_directory, invoice)
        invoice_abs_filepath = os.path.abspath(invoice_filepath)

        if invoice_abs_filepath.endswith(".jpg") or invoice_abs_filepath.endswith(".png"):
            invoice_img = cv2.imread(invoice_abs_filepath)
            invoice_text = pytesseract.image_to_string(invoice_img)
            return [invoice_text, invoice]
        
        elif invoice_abs_filepath.endswith(".pdf"):
            with open(invoice_abs_filepath, 'rb') as f:
                invoice_text = ""
                pdf = PdfFileReader(f) 
                i = 0
                numPages = pdf.getNumPages()
                while i < numPages:
                    invoice_text += pdf.getPage(i).extractText()
                    i+=1
                return [invoice_text, invoice]

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Extract text from all .jpg, .png, and .pdf files in directory
# RETURNS: list [list of strings, list of strings]
def extract_text_from_invoices(invoice_directory):
    invoices_texts = []
    invoices_filenames = []
    invoice_dir_list = os.listdir(invoice_directory)

    if __name__ == '__main__':
        pool = mp.Pool(mp.cpu_count())

        invoices_combined = [pool.apply(invoice_processor, args=(invoice, invoice_directory)) for invoice in invoice_dir_list]
        pool.close()  

        for invoice_combi in invoices_combined:
            invoices_texts.append(invoice_combi[0])
            invoices_filenames.append(invoice_combi[1])

        return [invoices_texts, invoices_filenames]

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':

    # Load model & vectorizer using pickle:
    os.chdir(r'D:\School\ProjectSE\Python\model')
    with open('invoice_classifier.pickle', 'rb') as model_file:
        model = pickle.load(model_file)
    with open('tfidf.pickle', 'rb') as tfidf_file:
        tfidf = pickle.load(tfidf_file)

    # Accept folder parameter
    folder = sys.argv[1]

    # Copy new invoices from external folder to local invoices folder for expansion of training data
    model_train_folder = r"D:\School\ProjectSE\Python\model\invoices"
    for file in os.listdir(folder):
        copy2(os.path.join(folder, file), model_train_folder)

    # Extract text from invoices and prepare seperate lists
    invoices_data = extract_text_from_invoices(folder)
    invoices_texts = invoices_data[0]
    invoices_filenames = invoices_data[1]

    # Tokenize texts from invoices and use to fit TFIDF vectorizer
    tokenized_invoices = tokenize_list(invoices_texts)
    transformed_invoices = tfidf.transform(tokenized_invoices)

    # Use model to predict transformed invoices
    predicitons = model.predict(transformed_invoices)

    # Print classification results for further use
    i = 0
    return_string = ""
    while i < len(predicitons):
        return_string += str(invoices_filenames[i]) + ":" + str(predicitons[i]) if i == len(predicitons)-1 else str(invoices_filenames[i]) + ":" + str(predicitons[i]) + ":"
        i+=1
    print(return_string)