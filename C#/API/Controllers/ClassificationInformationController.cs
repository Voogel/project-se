﻿using System;
using Microsoft.AspNetCore.Mvc;
using API.Models;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassificationInformationController : ControllerBase
    {
        /// <summary>
        /// HTTP POST endpoint: api/ClassificationInformation
        /// </summary>
        /// <returns>
        /// IActionResult
        /// </returns>
        /// <param name="classificationInformation"></param>
        [HttpPost]
        public IActionResult PostClassificationInformation(ClassificationInformation classificationInformation)
        {
            try
            {
                classificationInformation.CheckLocation();
                Classifier classifier = new Classifier(classificationInformation);
                var classifiedInvoices = classifier.Classify();
                return Ok(classifiedInvoices);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    }
}
