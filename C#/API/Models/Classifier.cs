﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace API.Models
{
    public class Classifier
    {
        private string invoiceFolder;
        private const string escapedQuote = "\"";

        /// <summary>
        /// Constructor for the Classifier class
        /// </summary>
        /// <param name="classificationInformation"></param>
        public Classifier(ClassificationInformation classificationInformation)
        {
            this.invoiceFolder = escapedQuote + classificationInformation.Location + escapedQuote;
        }



        /// <summary>
        /// Classifies invoices from folder defined by string invoiceFolder using Python script \ProjectSE\Python\model\invoice_analyzer.py
        /// </summary>
        /// <returns>
        /// List(Tuple(string, string))
        /// </returns>
        public List<Tuple<string,string>> Classify()
        {
            string classifierPythonFile = escapedQuote + @"D:\School\ProjectSE\Python\model\invoice_analyzer.py" + escapedQuote;
            string pythonExe = @"C:\Users\Simon V\AppData\Local\Programs\Python\Python38-32\python.exe";
            string argsString = classifierPythonFile + " " + this.invoiceFolder;

            ProcessStartInfo start = new ProcessStartInfo(pythonExe, classifierPythonFile);
            start.Arguments = argsString;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.CreateNoWindow = true;

            string result;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    result = reader.ReadToEnd();
                }
            }

            string[] splitResults = result.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
            string invoiceVal = null;
            string sentimentVal = null;
            List<Tuple<string, string>> tuples = new List<Tuple<string, string>>();

            for (int i = 0; i < splitResults.Length; i++)
            {
                if (i % 2 == 0)
                {
                    invoiceVal = Regex.Replace(splitResults[i], @"\t|\n|\r", "");
                }
                else
                {
                    sentimentVal = Regex.Replace(splitResults[i], @"\t|\n|\r", "");
                    sentimentVal = sentimentVal == "0" ? "safe" : "dangerous";
                    Tuple<string, string> tuple = new Tuple<string, string>(invoiceVal, sentimentVal);
                    tuples.Add(tuple);
                }
            }
            return tuples;
        }
    }
}
