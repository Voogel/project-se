﻿using API.Networking;
using System;
using System.Net;

namespace API.Models
{/// <summary>
/// 
/// </summary>
    public class ClassificationInformation
    {
        public long Id { get; set; }
        public string Location { get; set; }
        public bool ProcesImmediately { get; set; }

        /// <summary>
        /// Checks the location of serialized Location string by establishing a connection using the NetworkConnection class
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        public void CheckLocation()
        {
            try
            {
                var conn = new NetworkConnection(Location, new NetworkCredential(userName: "guest", password: ""));
            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
