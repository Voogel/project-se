﻿namespace GUI
{
    partial class MainGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectFolderButton = new System.Windows.Forms.Button();
            this.selectedFolderLabel = new System.Windows.Forms.Label();
            this.selectedFolderTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.applyFolderButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lastTrainingTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nextTrainingTextBox = new System.Windows.Forms.TextBox();
            this.nextModelTrainingLabel = new System.Windows.Forms.Label();
            this.applyIntervalButton = new System.Windows.Forms.Button();
            this.monthlyRadioButton = new System.Windows.Forms.RadioButton();
            this.weeklyRadioButton = new System.Windows.Forms.RadioButton();
            this.dailyRadioButton = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.trainingProgressBar = new System.Windows.Forms.ProgressBar();
            this.trainingStatsTextBox = new System.Windows.Forms.TextBox();
            this.trainManuallyButton = new System.Windows.Forms.Button();
            this.trainManuallyLabel = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // selectFolderButton
            // 
            this.selectFolderButton.Location = new System.Drawing.Point(6, 54);
            this.selectFolderButton.Name = "selectFolderButton";
            this.selectFolderButton.Size = new System.Drawing.Size(75, 23);
            this.selectFolderButton.TabIndex = 0;
            this.selectFolderButton.Text = "Select folder";
            this.selectFolderButton.UseVisualStyleBackColor = true;
            this.selectFolderButton.Click += new System.EventHandler(this.selectFolderButton_Click);
            // 
            // selectedFolderLabel
            // 
            this.selectedFolderLabel.AutoSize = true;
            this.selectedFolderLabel.Location = new System.Drawing.Point(3, 0);
            this.selectedFolderLabel.Name = "selectedFolderLabel";
            this.selectedFolderLabel.Size = new System.Drawing.Size(81, 13);
            this.selectedFolderLabel.TabIndex = 1;
            this.selectedFolderLabel.Text = "Selected folder:";
            this.selectedFolderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedFolderTextBox
            // 
            this.selectedFolderTextBox.Location = new System.Drawing.Point(6, 16);
            this.selectedFolderTextBox.Name = "selectedFolderTextBox";
            this.selectedFolderTextBox.ReadOnly = true;
            this.selectedFolderTextBox.Size = new System.Drawing.Size(166, 20);
            this.selectedFolderTextBox.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.applyFolderButton);
            this.panel1.Controls.Add(this.selectedFolderLabel);
            this.panel1.Controls.Add(this.selectedFolderTextBox);
            this.panel1.Controls.Add(this.selectFolderButton);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 83);
            this.panel1.TabIndex = 3;
            // 
            // applyFolderButton
            // 
            this.applyFolderButton.Location = new System.Drawing.Point(200, 13);
            this.applyFolderButton.Name = "applyFolderButton";
            this.applyFolderButton.Size = new System.Drawing.Size(75, 23);
            this.applyFolderButton.TabIndex = 3;
            this.applyFolderButton.Text = "Apply";
            this.applyFolderButton.UseVisualStyleBackColor = true;
            this.applyFolderButton.Click += new System.EventHandler(this.applyFolderButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lastTrainingTextBox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.nextTrainingTextBox);
            this.panel2.Controls.Add(this.nextModelTrainingLabel);
            this.panel2.Controls.Add(this.applyIntervalButton);
            this.panel2.Controls.Add(this.monthlyRadioButton);
            this.panel2.Controls.Add(this.weeklyRadioButton);
            this.panel2.Controls.Add(this.dailyRadioButton);
            this.panel2.Location = new System.Drawing.Point(12, 157);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(286, 139);
            this.panel2.TabIndex = 4;
            // 
            // lastTrainingTextBox
            // 
            this.lastTrainingTextBox.Location = new System.Drawing.Point(3, 105);
            this.lastTrainingTextBox.Name = "lastTrainingTextBox";
            this.lastTrainingTextBox.ReadOnly = true;
            this.lastTrainingTextBox.Size = new System.Drawing.Size(261, 20);
            this.lastTrainingTextBox.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Last model training occurred at:";
            // 
            // nextTrainingTextBox
            // 
            this.nextTrainingTextBox.Location = new System.Drawing.Point(3, 52);
            this.nextTrainingTextBox.Name = "nextTrainingTextBox";
            this.nextTrainingTextBox.ReadOnly = true;
            this.nextTrainingTextBox.Size = new System.Drawing.Size(261, 20);
            this.nextTrainingTextBox.TabIndex = 6;
            // 
            // nextModelTrainingLabel
            // 
            this.nextModelTrainingLabel.AutoSize = true;
            this.nextModelTrainingLabel.Location = new System.Drawing.Point(0, 36);
            this.nextModelTrainingLabel.Name = "nextModelTrainingLabel";
            this.nextModelTrainingLabel.Size = new System.Drawing.Size(159, 13);
            this.nextModelTrainingLabel.TabIndex = 5;
            this.nextModelTrainingLabel.Text = "Next model training will occur at:";
            // 
            // applyIntervalButton
            // 
            this.applyIntervalButton.Location = new System.Drawing.Point(200, 3);
            this.applyIntervalButton.Name = "applyIntervalButton";
            this.applyIntervalButton.Size = new System.Drawing.Size(75, 23);
            this.applyIntervalButton.TabIndex = 3;
            this.applyIntervalButton.Text = "Apply";
            this.applyIntervalButton.UseVisualStyleBackColor = true;
            this.applyIntervalButton.Click += new System.EventHandler(this.applyIntervalButton_Click);
            // 
            // monthlyRadioButton
            // 
            this.monthlyRadioButton.AutoSize = true;
            this.monthlyRadioButton.Location = new System.Drawing.Point(132, 6);
            this.monthlyRadioButton.Name = "monthlyRadioButton";
            this.monthlyRadioButton.Size = new System.Drawing.Size(62, 17);
            this.monthlyRadioButton.TabIndex = 2;
            this.monthlyRadioButton.TabStop = true;
            this.monthlyRadioButton.Text = "Monthly";
            this.monthlyRadioButton.UseVisualStyleBackColor = true;
            this.monthlyRadioButton.CheckedChanged += new System.EventHandler(this.monthlyRadioButton_CheckedChanged);
            // 
            // weeklyRadioButton
            // 
            this.weeklyRadioButton.AutoSize = true;
            this.weeklyRadioButton.Location = new System.Drawing.Point(65, 6);
            this.weeklyRadioButton.Name = "weeklyRadioButton";
            this.weeklyRadioButton.Size = new System.Drawing.Size(61, 17);
            this.weeklyRadioButton.TabIndex = 1;
            this.weeklyRadioButton.TabStop = true;
            this.weeklyRadioButton.Text = "Weekly";
            this.weeklyRadioButton.UseVisualStyleBackColor = true;
            this.weeklyRadioButton.CheckedChanged += new System.EventHandler(this.weeklyRadioButton_CheckedChanged);
            // 
            // dailyRadioButton
            // 
            this.dailyRadioButton.AutoSize = true;
            this.dailyRadioButton.Location = new System.Drawing.Point(11, 6);
            this.dailyRadioButton.Name = "dailyRadioButton";
            this.dailyRadioButton.Size = new System.Drawing.Size(48, 17);
            this.dailyRadioButton.TabIndex = 0;
            this.dailyRadioButton.TabStop = true;
            this.dailyRadioButton.Text = "Daily";
            this.dailyRadioButton.UseVisualStyleBackColor = true;
            this.dailyRadioButton.CheckedChanged += new System.EventHandler(this.dailyRadioButton_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.trainingProgressBar);
            this.panel3.Controls.Add(this.trainingStatsTextBox);
            this.panel3.Controls.Add(this.trainManuallyButton);
            this.panel3.Controls.Add(this.trainManuallyLabel);
            this.panel3.Location = new System.Drawing.Point(538, 11);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(286, 285);
            this.panel3.TabIndex = 5;
            // 
            // trainingProgressBar
            // 
            this.trainingProgressBar.Location = new System.Drawing.Point(54, 124);
            this.trainingProgressBar.Name = "trainingProgressBar";
            this.trainingProgressBar.Size = new System.Drawing.Size(165, 18);
            this.trainingProgressBar.TabIndex = 3;
            this.trainingProgressBar.Visible = false;
            // 
            // trainingStatsTextBox
            // 
            this.trainingStatsTextBox.Location = new System.Drawing.Point(11, 43);
            this.trainingStatsTextBox.Multiline = true;
            this.trainingStatsTextBox.Name = "trainingStatsTextBox";
            this.trainingStatsTextBox.ReadOnly = true;
            this.trainingStatsTextBox.Size = new System.Drawing.Size(261, 185);
            this.trainingStatsTextBox.TabIndex = 2;
            // 
            // trainManuallyButton
            // 
            this.trainManuallyButton.Location = new System.Drawing.Point(132, 7);
            this.trainManuallyButton.Name = "trainManuallyButton";
            this.trainManuallyButton.Size = new System.Drawing.Size(75, 23);
            this.trainManuallyButton.TabIndex = 1;
            this.trainManuallyButton.Text = "Train model";
            this.trainManuallyButton.UseVisualStyleBackColor = true;
            this.trainManuallyButton.Click += new System.EventHandler(this.trainManuallyButton_Click);
            // 
            // trainManuallyLabel
            // 
            this.trainManuallyLabel.AutoSize = true;
            this.trainManuallyLabel.Location = new System.Drawing.Point(11, 12);
            this.trainManuallyLabel.Name = "trainManuallyLabel";
            this.trainManuallyLabel.Size = new System.Drawing.Size(106, 13);
            this.trainManuallyLabel.TabIndex = 0;
            this.trainManuallyLabel.Text = "Train model manually";
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            // 
            // MainGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 307);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "MainGUI";
            this.Text = "Model trainer";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button selectFolderButton;
        private System.Windows.Forms.Label selectedFolderLabel;
        private System.Windows.Forms.TextBox selectedFolderTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton monthlyRadioButton;
        private System.Windows.Forms.RadioButton weeklyRadioButton;
        private System.Windows.Forms.RadioButton dailyRadioButton;
        private System.Windows.Forms.Button applyIntervalButton;
        private System.Windows.Forms.TextBox nextTrainingTextBox;
        private System.Windows.Forms.Label nextModelTrainingLabel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button trainManuallyButton;
        private System.Windows.Forms.Label trainManuallyLabel;
        private System.Windows.Forms.TextBox trainingStatsTextBox;
        private System.Windows.Forms.ProgressBar trainingProgressBar;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Button applyFolderButton;
        private System.Windows.Forms.TextBox lastTrainingTextBox;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

