﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class MainGUI : Form
    {
        private string intervalOld;
        private string intervalNew;
        private bool intervalChanged;
        private DateTime nextTraining;
        private string selectedFolder;
        private string selectedFolderNew;
        private Configuration config;
        private Thread scheduledTrainingThread;
        public MainGUI()
        {
            // Extract configurations from App.config for instantiating UI elements
            config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            string folderConfig = ConfigurationManager.AppSettings["Folder"];
            string intervalConfig = ConfigurationManager.AppSettings["Interval"];
            string lastTrainedConfig = ConfigurationManager.AppSettings["LastTrained"];

            // Initialize GUI elements
            InitializeComponent();
            selectedFolder = folderConfig;
            selectedFolderTextBox.Text = folderConfig;
            intervalOld = intervalConfig;
            nextTraining = calcNextTraining(intervalConfig);
            nextTrainingTextBox.Text = nextTraining.ToString();
            lastTrainingTextBox.Text = lastTrainedConfig;
            if (intervalConfig == "daily") dailyRadioButton.Checked = true;
            else if (intervalConfig == "weekly") weeklyRadioButton.Checked = true;
            else if (intervalConfig == "monthly") monthlyRadioButton.Checked = true;

            // Start automatic training scheduler thread
            spinOffNewTrainingThread(this.selectedFolder, this.intervalOld, this.nextTraining);
        }






        // GUI events ================================================================================================================================================
        /// <summary>
        /// Click event of 'Select folder' button
        /// </summary>
        private void selectFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog()
            {
                ShowNewFolderButton = true,
                Description = "Select folder with traindata"
            };

            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                selectedFolderNew = dialog.SelectedPath;
                selectedFolderTextBox.Text = selectedFolderNew;
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Click event of 'Apply' button for selected folder
        /// </summary>
        private void applyFolderButton_Click(object sender, EventArgs e)
        {
            if (!selectedFolderNew.Equals(selectedFolder))
            {
                selectedFolder = selectedFolderNew;
                UpdateConfig("Folder", selectedFolderNew);
                spinOffNewTrainingThread(this.selectedFolder, this.intervalOld, this.nextTraining);
            }

        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Checked event of radiobutton 'daily'
        /// </summary>
        private void dailyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton buttonvalue = (RadioButton)sender;
            if (buttonvalue.Checked)
            {
                intervalNew = "daily";
                intervalChanged = true;
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Checked event of radiobutton 'weekly'
        /// </summary>
        private void weeklyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton buttonvalue = (RadioButton)sender;
            if (buttonvalue.Checked)
            {
                intervalNew = "weekly";
                intervalChanged = true;
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Checked event of radiobutton 'daily'
        /// </summary>>
        private void monthlyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton buttonvalue = (RadioButton)sender;
            if (buttonvalue.Checked)
            {
                intervalNew = "monthly";
                intervalChanged = true;
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Click event of 'Apply' button for selected interval
        /// </summary>
        private void applyIntervalButton_Click(object sender, EventArgs e)
        {
            if (intervalChanged && intervalOld != intervalNew)
            {
                intervalChanged = false;
                intervalOld = intervalNew;
                nextTraining = calcNextTraining(intervalNew);
                nextTrainingTextBox.Text = nextTraining.ToString();
                UpdateConfig("Interval", intervalNew);
                spinOffNewTrainingThread(this.selectedFolder, this.intervalOld, this.nextTraining);
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Click event of 'Train' button to manually train machine learning model
        /// </summary>
        private void trainManuallyButton_Click(object sender, EventArgs e)
        {
            trainingStatsTextBox.Text = "";
            trainingProgressBar.Visible = true;
            trainingProgressBar.Style = ProgressBarStyle.Marquee;
            Thread thread = new Thread(new ThreadStart(TrainModelReadResult));
            thread.Start();
        }
        // ===========================================================================================================================================================











        // Internal logic ============================================================================================================================================
        /// <summary>
        /// Calculates the next training moment for machine learning model, based on the interval parameter
        /// </summary>
        /// <returns>
        /// DateTime
        /// </returns>
        /// <param name="interval"></param>
        private DateTime calcNextTraining(string interval)
        {
            DateTime next = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 3, 0, 0);

            if (interval == "daily")
            {
                next = next.AddDays(1);
            }
            else if (interval == "weekly")
            {
                next = next.AddDays(1);
                while (next.DayOfWeek != DayOfWeek.Sunday) next = next.AddDays(1);
            }
            else if (interval == "monthly")
            {
                int month = next.Month;
                next = next.AddDays(1);
                while (next.Month == month) next = next.AddDays(1);
                while (next.DayOfWeek != DayOfWeek.Sunday) next = next.AddDays(1);
            }
            return next;
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Updates configuration value of given key
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private void UpdateConfig(string key, string value)
        {
            config.AppSettings.Settings.Remove(key);
            config.AppSettings.Settings.Add(key, value);
            config.Save(ConfigurationSaveMode.Modified);
        }
        // ===========================================================================================================================================================










        // Threading + delegates =====================================================================================================================================
        /// <summary>
        /// Starts new thread to automate the training of a new machine learning model. 
        /// Uses the contents of the 'folder' parameter as train data, the 'interval' parameter to specify the interval, 
        /// and the 'nextTraining' parameter as the next training moment
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        /// <param name="folder"></param>
        /// <param name="interval"></param>
        /// <param name="nextTraining"></param>
        private void spinOffNewTrainingThread(string folder, string interval, DateTime nextTraining)
        {
            if (scheduledTrainingThread == null)
            {
                scheduledTrainingThread = new Thread(() => trainingScheduler(folder, interval, nextTraining));
                scheduledTrainingThread.Start();
            }
            else
            {
                scheduledTrainingThread.Abort();
                scheduledTrainingThread = new Thread(() => trainingScheduler(folder, interval, nextTraining));
                scheduledTrainingThread.Start();
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Sets GUI elements and interval variables to their correct values
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        /// <param name="folder"></param>
        /// <param name="interval"></param>
        /// <param name="nextTraining"></param>
        public void trainingScheduler(string folder, string interval, DateTime nextTraining)
        {
            DateTime innerNextTraining = nextTraining;
            while (true)
            {
                if (DateTime.Now > innerNextTraining)
                {
                    DateTime next = calcNextTraining(interval);
                    this.nextTraining = next;
                    innerNextTraining = next;
                    setNextTrainingTextBox(next);
                    TrainModel(folder);
                    DateTime now = DateTime.Now;
                    setLastTrainingTextBox(now);
                    UpdateConfig("LastTrained", now.ToString());
                }
                else Thread.Sleep(30000);
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------

        internal delegate void SetNextTrainingTextBoxDelegate(DateTime nextTraining);
        /// <summary>
        /// Method to invoke delegate of 'Next training TextBox' by different thread
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        /// <param name="nextTraining"></param>
        private void setNextTrainingTextBox(DateTime nextTraining)
        {
            if (InvokeRequired) Invoke(new SetNextTrainingTextBoxDelegate(setNextTrainingTextBox), nextTraining);
            else nextTrainingTextBox.Text = nextTraining.ToString();
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------

        internal delegate void SetLastTrainingTextBoxDelegate(DateTime lastTraining);
        /// <summary>
        /// Method to invoke delegate of 'Last training TextBox' by different thread
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        /// <param name="lastTraining"></param>
        private void setLastTrainingTextBox(DateTime lastTraining)
        {
            if (InvokeRequired) Invoke(new SetLastTrainingTextBoxDelegate(setLastTrainingTextBox), lastTraining);
            else lastTrainingTextBox.Text = lastTraining.ToString();
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------

        internal delegate void SetDataSourceDelegate(string result);
        /// <summary>
        /// Method to invoke delegate of 'DataSource' by different thread. 
        /// Accepts return string of executed Python script to show in GUI.
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        /// <param name="result"></param>
        private void setDataSource(string result)
        {
            if (InvokeRequired)
            {
                Invoke(new SetDataSourceDelegate(setDataSource), result);
            }
            else
            {
                trainingStatsTextBox.Text = result;
                trainingProgressBar.Visible = false;
                DateTime now = DateTime.Now;
                setLastTrainingTextBox(now);
                UpdateConfig("LastTrained", now.ToString());
            }
        }
        // ===========================================================================================================================================================










        // Python ====================================================================================================================================================
        /// <summary>
        /// Trains machine learning model using Python script and waits for results to show in the GUI
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        private void TrainModelReadResult()
        {
            const string escapedQuote = "\"";
            string classifierPythonFile = escapedQuote + @"D:\School\ProjectSE\Python\model\classification_model_trainer.py" + escapedQuote;
            string pythonExe = @"C:\Users\Simon V\AppData\Local\Programs\Python\Python38-32\python.exe";
            string argsString = classifierPythonFile + " " + selectedFolder;

            ProcessStartInfo start = new ProcessStartInfo(pythonExe, classifierPythonFile);
            start.Arguments = argsString;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;

            string result;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    result = reader.ReadToEnd();
                }
            }
            setDataSource(result);
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Trains machine learning model using Python script on contents of specified folder parameter
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        /// <param name="selectedFolder"></param>
        private void TrainModel(string selectedFolder)
        {
            const string escapedQuote = "\"";
            string classifierPythonFile = escapedQuote + @"D:\School\ProjectSE\Python\model\classification_model_trainer.py" + escapedQuote;
            string pythonExe = @"C:\Users\Simon V\AppData\Local\Programs\Python\Python38-32\python.exe";
            string argsString = classifierPythonFile + " " + selectedFolder;

            ProcessStartInfo start = new ProcessStartInfo(pythonExe, classifierPythonFile);
            start.Arguments = argsString;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            Process.Start(start);
        }

        // ===========================================================================================================================================================

    }
}









